# vue-auto-scale

#### 介绍
vue大屏适配缩放组件

#### 软件架构
##### 支持vue2.7+ vue2.7 以下需安装 `` cnpm i @vue/composition-api -S ``


#### 安装教程

```bash
npm i vue-auto-scale -S
or
yarn add vue-auto-scale
```

#### 使用说明
***使用时需要将页面中单位全部换成px 百分比不受影响***
```html
<template>
    <vue-auto-scale
            :width="1920"
            :height="1080"
            :delay="100"
            :scale-type="scaleType"
            :reverse-scaling-ids="['div2']"
            @change="scaleChange"
    >
<!--    被缩放的元素-->
    <div class="my-demo">
<!--        不想被缩放的元素比如地图等 当然这个也可以写在 vue-auto-scale 标签外面-->
        <div id="div2"></div>
    </div>
  </vue-auto-scale>
</template>
```

``` javascript
<script setup>
import VueAutoScale from "vue-auto-scale";

const scaleChange = (scale, scale1) => {
 console.log('缩放比例', scale)
 console.log('反向缩放比例', scale1)
}
</script>
```


## 属性
- ` width ` 宽度(设计搞宽度) 默认 `1920`
- ` height ` 高度(设计稿高度) 默认 `1080`
- ` delay `自适应缩放防抖延迟时间（ms） 默认 `100`
- ` scaleType `缩放类型 `1`: 强制铺满 `2`: 按比例缩放 `3`: 适应宽度 `4`: 适应高度 默认值` 1 `
- ` reverse-scaling-ids `不被缩放的元素id 可以为数组或者字符串 默认为 `null ` 
- ` parent` 获取宽高的时候是否使用父元素的宽高 默认 `false` 
#### **以上参数均支持响应式**

# 如果有其他需求可以在gitee上提出,后续会新增和优化
