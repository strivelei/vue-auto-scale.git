/**
 * 防抖函数
 * @param fn 执行的函数
 * @param delay 延迟时间
 * @returns {(function(): void)|*}
 */
export function debounce(fn, delay) {
    let timer;
    return function () {
        const th = this;
        const args = arguments;
        if (timer) {
            clearTimeout(timer);
        }
        timer = setTimeout(function () {
            timer = null;
            fn.apply(th, args);
        }, delay);
    };
}
