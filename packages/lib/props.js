/**
 * width      宽
 * height     高
 * scaleType  缩放类型 1: 按比例适配 2: 强制铺满
 * delay      自适应缩放防抖延迟时间（ms）
 * reverseScalingIds      不被缩放的元素id
 */
export default {
    /**
     * 设计稿宽度
     */
    width: {
        type: [Number, String],
        default: 1920,
    },
    /**
     * 设计稿高度
     */
    height: {
        type: [Number, String],
        default: 1080,
    },
    /**
     * 缩放类型 1: 强制铺满 2: 按比例缩放 3: 适应宽度 4: 适应高度 默认值为1
     */
    scaleType: {
        type: Number,
        default: 1,
        validator(value) {
            // The value must match one of these strings
            return [1, 2, 3, 4].includes(value)
        }
    },
    /**
     * 自适应缩放防抖延迟时间（ms） 默认 100
     */
    delay: {
        type: Number,
        default: 100,
    },
    /**
     * 不被缩放的元素id
     */
    reverseScalingIds: {
        type: [Array, String],
        default: null
    },
    /**
     * 获取宽高的时候是否使用父元素的宽高 默认false
     */
    parent: {
        type: Boolean,
        default: false
    }
};
