import VueAutoScale from './lib/index.vue';

/* istanbul ignore next */
VueAutoScale.install = function(Vue) {
    Vue.component(VueAutoScale.name, VueAutoScale);
};

export default VueAutoScale;
